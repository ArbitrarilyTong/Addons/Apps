# Add apps here to have them built into the ROM
PRODUCT_PACKAGES += \
    DuckDuckGo \
    MotorolaCalculator \
    MotorolaCalendar \
    MotorolaClock \
    MotorolaCompass \
    MotorolaContacts \
    MotorolaDialer \
    MotorolaGallery \
    MotorolaMessaging \
    MotorolaStylus \
    MotorolaWidgets

# Remove default music app
PRODUCT_DEL_PACKAGES := \
    Eleven