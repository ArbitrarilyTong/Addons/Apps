# ArbitrarilyTong Pre-built Apps

Apps:

- DuckDuckGo
- MotorolaCalculator
- MotorolaCalendar
- MotorolaClock
- MotorolaCompass
- MotorolaContacts
- MotorolaDialer
- MotorolaGallery
- MotorolaMessaging
- MotorolaStylus
- MotorolaWidgets

Remove:

- Eleven (LineageOS default music app)

Credits:

- [Android12 系统的裁剪编译规则](https://blog.51cto.com/u_15243273/5426786)
  - The commit is on [ArbitrarilyTong/tong_build](https://github.com/ArbitrarilyTong/tong_build/commit/9a88610036b7133471d61227f27b0847d13aa6a6)